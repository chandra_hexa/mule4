<OTA_ResRetrieveRS EchoToken="" TimeStamp="2019-07-15T16:02:12.822+08:00" Version="1.0">
	<Success/>   
	<ReservationsList>
		<UniqueID Type="4" ID="64297UT025169"/>
		<RoomStays>
			<RoomStay RoomStayStatus="Arrived">
				<RoomTypes>
					<RoomType RoomTypeID="391" RoomTypeCode="One of a Kind" RoomID="11004">
						<RoomDescription>
							<Text>3003 H</Text>
						</RoomDescription>
					</RoomType>
				</RoomTypes>
				<GuestCounts>
					<GuestCount AgeQualifyingCode="10" Count="3"/>
					<GuestCount AgeQualifyingCode="8" Count="0"/>
					<GuestCount AgeQualifyingCode="7"/>
				</GuestCounts>
				<TimeSpan End="2019-07-09T13:00:00" Start="2019-07-09T12:30:00"/>
				<Total AmountAfterTax="192.7600"/>
				<BasicPropertyInfo HotelCode="4" HotelName="Lyf Funan - Demo"/>
				<Memberships>
					<Membership ProgramCode=""/>
				</Memberships>
			</RoomStay>
		</RoomStays>
		<ResGuests>
			<ResGuest>
				<Profiles>
					<ProfileInfo>
						<UniqueID Type="" ID="7826744">
							<Profile>
								<Customer>
									<PersonName>
										<GivenName>An doi ten</GivenName>
										<SurName>Nguyen</SurName>
									</PersonName>
									<Telehphone PhoneTechType="1"/>
									<Telehphone PhoneTechType="5"/>
									<Address>
										<CityName>3003 H</CityName>
									</Address>
									<Comments/>
								</Customer>
							</Profile>
						</UniqueID>
					</ProfileInfo>
				</Profiles>
			</ResGuest>
		</ResGuests>
		<TPA_Extensions>
			<Accomm>0.0000</Accomm>
			<AccommBalance>0.0000</AccommBalance>
			<AccountBalance>0.0000</AccountBalance>
			<AccountId>7849169</AccountId>
			<BillCategoryId>0</BillCategoryId>
			<BillToDetails>
				<BillTo>
					<AccType>Accomm</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Accomm</Description>
					<EntityId>7444558</EntityId>
					<EntityName>Miss An doi ten Nguyen</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Extras</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Incidentals</Description>
					<EntityId>7444558</EntityId>
					<EntityName>Miss An doi ten Nguyen</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Gas</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 3</Description>
					<EntityId>7444558</EntityId>
					<EntityName>Miss An doi ten Nguyen</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Electricity</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Bonds</Description>
					<EntityId>7444558</EntityId>
					<EntityName>Miss An doi ten Nguyen</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Water</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 5</Description>
					<EntityId>7444558</EntityId>
					<EntityName>Miss An doi ten Nguyen</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BookingSource>Website - ASR</BookingSource>
			<BookingSourceGroupingId>0</BookingSourceGroupingId>
			<BookingSourceId>4</BookingSourceId>
			<BookingType>Accommodation</BookingType>
			<CancelledBy>0</CancelledBy>
			<CancelledDate>1900-01-01T00:00:00</CancelledDate>
			<CategoryId>391</CategoryId>
			<ChargesAccomm>192.7600</ChargesAccomm>
			<ChargesExtras>0</ChargesExtras>
			<ChargesOther>0</ChargesOther>
			<CheckInDate>2019-07-09T12:30:48.073</CheckInDate>
			<CheckOutDate>1900-01-01T00:00:00</CheckOutDate>
			<CommissionPercentage>0.0000</CommissionPercentage>
			<CompanyId>0</CompanyId>
			<ConfirmedBy>2</ConfirmedBy>
			<ConfirmedDate>2019-07-09T11:55:57.657</ConfirmedDate>
			<CreationDate>2019-07-09T11:55:57.5</CreationDate>
			<CurrencyCode>SGD</CurrencyCode>
			<DailyRevenueBreakDown>
				<DailyRevenueBreakDown>
					<AdditionalsAmount>0.0000</AdditionalsAmount>
					<Currency>SGD</Currency>
					<Date>2019-07-09T00:00:00</Date>
					<Discount>0.0000</Discount>
					<DynamicAmount>0.0000</DynamicAmount>
					<ExclusiveTax>33.4600</ExclusiveTax>
					<LinkedRateAdjustmentAmount>-17.7000</LinkedRateAdjustmentAmount>
					<PackageAmount>0.0000</PackageAmount>
					<Period>2019</Period>
					<Rate>177.0000</Rate>
					<RateId>39</RateId>
					<RateTable>SG 710</RateTable>
					<RateType>Smart - IncBF</RateType>
					<ResId>201093</ResId>
					<TotalRate>192.7600</TotalRate>
					<XNightsDiscount>0.0000</XNightsDiscount>
				</DailyRevenueBreakDown>
			</DailyRevenueBreakDown>
			<DepositAmount>0.0000</DepositAmount>
			<DepositAmountSecond>0.0000</DepositAmountSecond>
			<DepositDateRequired>2019-07-09T00:00:00</DepositDateRequired>
			<DepositRequiredBy>2019-07-09T00:00:00</DepositRequiredBy>
			<DiscountAmount>0.0000</DiscountAmount>
			<DiscountCodeId>0</DiscountCodeId>
			<EventFinish>1900-01-01T00:00:00</EventFinish>
			<EventStart>1900-01-01T00:00:00</EventStart>
			<Extras>0</Extras>
			<ExtrasBalance>0</ExtrasBalance>
			<Fixed>false</Fixed>
			<GroupMaster>false</GroupMaster>
			<GroupMasterAccountId>0</GroupMasterAccountId>
			<GroupResId>0</GroupResId>
			<LastModified>2019-07-09T14:46:11.86</LastModified>
			<ListofCorresspondences>
				<Correspondence>
					<Description>RegistrationForm_An doi ten_Nguyen_09072019123050.pdf</Description>
					<EnteredDate>2019-07-09T04:30:51</EnteredDate>
					<Id>299317</Id>
					<Type>In</Type>
				</Correspondence>
			</ListofCorresspondences>
			<ListofHouseKeepingTasks/>
			<ListofReceipts>
				<ReceiptLite>
					<DateOfReceipt>2019-07-09T00:00:00</DateOfReceipt>
				</ReceiptLite>
			</ListofReceipts>
			<LongTerm>false</LongTerm>
			<MadeBy>RMSOnline</MadeBy>
			<MarketSegment>Airline Crew Group</MarketSegment>
			<MarketSegmentId>8</MarketSegmentId>
			<MealPlanId>0</MealPlanId>
			<ModifiedBy>toan.nguyen</ModifiedBy>
			<Nights>1</Nights>
			<OTARef2>219070900001</OTARef2>
			<POSOnGroupMaster>false</POSOnGroupMaster>
			<Package>0.0000</Package>
			<PaymentAccomm>192.7600</PaymentAccomm>
			<PaymentExtras>0</PaymentExtras>
			<PaymentMode>Complimentary/House Use Rm only</PaymentMode>
			<PaymentOther>0</PaymentOther>
			<PrimaryClient>
				<AccountBalance>0.0000</AccountBalance>
				<AccountId>7826744</AccountId>
				<ActivitiesDisabled>false</ActivitiesDisabled>
				<AnniversaryDate>1900-01-01T00:00:00</AnniversaryDate>
				<AvgIncome>0</AvgIncome>
				<BlackListed>false</BlackListed>
				<ClientId>7444558</ClientId>
				<ClientType>Client</ClientType>
				<CompId>0</CompId>
				<ConsentToUseCreditCard>false</ConsentToUseCreditCard>
				<CreatedById>0</CreatedById>
				<CreationDate>1900-01-01T00:00:00</CreationDate>
				<DiscountId>0</DiscountId>
				<EDMFilter1OptOut>false</EDMFilter1OptOut>
				<EDMFilter2OptOut>false</EDMFilter2OptOut>
				<EDMFilter3OptOut>false</EDMFilter3OptOut>
				<EmailOptOut>false</EmailOptOut>
				<GDPRPrivacyOptIn>false</GDPRPrivacyOptIn>
				<InsuranceExpDate>1900-01-01T00:00:00</InsuranceExpDate>
				<LastModified>1900-01-01T00:00:00</LastModified>
				<LastTariffTypeId>0</LastTariffTypeId>
				<LastVisit>1900-01-01T00:00:00</LastVisit>
				<MarketingOptOut>false</MarketingOptOut>
				<ModifiedById>0</ModifiedById>
				<NoOfNights>0</NoOfNights>
				<NoOfVisits>0</NoOfVisits>
				<PrefAppartmentId>0</PrefAppartmentId>
				<PrivacyNoMailOuts>false</PrivacyNoMailOuts>
				<RVLengthId>0</RVLengthId>
				<RVSlideId>0</RVSlideId>
				<RVTypeId>0</RVTypeId>
				<RVYear>0</RVYear>
				<ResTypeId>0</ResTypeId>
				<Retailer>false</Retailer>
				<SMSOptOut>false</SMSOptOut>
				<SubResTypeId>0</SubResTypeId>
				<TotalIncome>0</TotalIncome>
			</PrimaryClient>
			<ProjectedRevenue>
				<DailyBreakdown>
					<ProjectedRevenueBreakdown>
						<Discount>0.0000</Discount>
						<Revenue>192.7600</Revenue>
						<Tax>33.4600</Tax>
						<TheDate>2019-07-09T00:00:00</TheDate>
					</ProjectedRevenueBreakdown>
				</DailyBreakdown>
			</ProjectedRevenue>
			<RMSOnlineConfirmationNo>16570862</RMSOnlineConfirmationNo>
			<RateCode>Smart - IncBF</RateCode>
			<RateCodeId>659</RateCodeId>
			<RateOnGroup>true</RateOnGroup>
			<ResTypeId>0</ResTypeId>
			<RoomMoveHeader>false</RoomMoveHeader>
			<RoomMoveHeaderId>0</RoomMoveHeaderId>
			<SecondDepositRequiredBy>1900-01-01T00:00:00</SecondDepositRequiredBy>
			<SecondDiscountAmount>0.0000</SecondDiscountAmount>
			<SubMarketSegmentId>0</SubMarketSegmentId>
			<TariffAmount>159.3000</TariffAmount>
			<TariffOnGroup>false</TariffOnGroup>
			<TariffPosted>true</TariffPosted>
			<TaxAmount>33.4600</TaxAmount>
			<TotalDynamicPrice>0.0000</TotalDynamicPrice>
			<TravelAgent>Sabre SynXis HTNG</TravelAgent>
			<TravelAgentId>2</TravelAgentId>
			<WholesaleId>0</WholesaleId>
		</TPA_Extensions>
		<ResGlobalInfo>
			<HotelReservationIDs>
				<HotelReservationID ResID_Type="14" ResID_Value="64297UT025169"/>
				<HotelReservationID ResID_Type="10"/>
			</HotelReservationIDs>
		</ResGlobalInfo>
	</ReservationsList>
	<ReservationsList>
		<UniqueID Type="4" ID="64297UT025189"/>
		<RoomStays>
			<RoomStay RoomStayStatus="Arrived">
				<RoomTypes>
					<RoomType RoomTypeID="389" RoomTypeCode="0DLX LFS" RoomID="10643">
						<RoomDescription>
							<Text>ngoc123</Text>
						</RoomDescription>
					</RoomType>
				</RoomTypes>
				<GuestCounts>
					<GuestCount AgeQualifyingCode="10" Count="1"/>
					<GuestCount AgeQualifyingCode="8" Count="0"/>
					<GuestCount AgeQualifyingCode="7"/>
				</GuestCounts>
				<TimeSpan End="2019-07-09T17:00:00" Start="2019-07-09T16:56:00"/>
				<Total AmountAfterTax="508.2000"/>
				<BasicPropertyInfo HotelCode="4" HotelName="Lyf Funan - Demo"/>
				<Memberships>
					<Membership ProgramCode=""/>
				</Memberships>
			</RoomStay>
		</RoomStays>
		<ResGuests>
			<ResGuest>
				<Profiles>
					<ProfileInfo>
						<UniqueID Type="" ID="7849179">
							<Profile>
								<Customer>
									<PersonName>
										<GivenName>An doi ten</GivenName>
										<SurName>Nga</SurName>
									</PersonName>
									<Telehphone PhoneTechType="1"/>
									<Telehphone PhoneTechType="5"/>
									<Address>
										<CityName>ngoc123</CityName>
									</Address>
									<Comments/>
								</Customer>
							</Profile>
						</UniqueID>
					</ProfileInfo>
				</Profiles>
			</ResGuest>
		</ResGuests>
		<TPA_Extensions>
			<Accomm>0.0000</Accomm>
			<AccommBalance>0.0000</AccommBalance>
			<AccountBalance>0.0000</AccountBalance>
			<AccountId>7849194</AccountId>
			<BillCategoryId>0</BillCategoryId>
			<BillToDetails>
				<BillTo>
					<AccType>Accomm</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Accomm</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Extras</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Incidentals</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Gas</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 3</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Electricity</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Bonds</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Water</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 5</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BookingSource>Website - ASR</BookingSource>
			<BookingSourceGroupingId>0</BookingSourceGroupingId>
			<BookingSourceId>4</BookingSourceId>
			<BookingType>Accommodation</BookingType>
			<CancelledBy>0</CancelledBy>
			<CancelledDate>1900-01-01T00:00:00</CancelledDate>
			<CategoryId>389</CategoryId>
			<ChargesAccomm>566.5000</ChargesAccomm>
			<ChargesExtras>0</ChargesExtras>
			<ChargesOther>0</ChargesOther>
			<CheckInDate>2019-07-09T16:56:31.73</CheckInDate>
			<CheckOutDate>1900-01-01T00:00:00</CheckOutDate>
			<CommissionPercentage>0.0000</CommissionPercentage>
			<CompanyId>0</CompanyId>
			<ConfirmedBy>2</ConfirmedBy>
			<ConfirmedDate>2019-07-09T16:43:27.73</ConfirmedDate>
			<CreationDate>2019-07-09T16:43:27.59</CreationDate>
			<CurrencyCode>SGD</CurrencyCode>
			<DailyRevenueBreakDown>
				<DailyRevenueBreakDown>
					<AdditionalsAmount>0.0000</AdditionalsAmount>
					<Currency>SGD</Currency>
					<Date>2019-07-09T00:00:00</Date>
					<Discount>0.0000</Discount>
					<DynamicAmount>0.0000</DynamicAmount>
					<ExclusiveTax>88.2000</ExclusiveTax>
					<LinkedRateAdjustmentAmount>-180.0000</LinkedRateAdjustmentAmount>
					<PackageAmount>0.0000</PackageAmount>
					<Period>2019</Period>
					<Rate>600.0000</Rate>
					<RateId>104</RateId>
					<RateTable>SG 600</RateTable>
					<RateType>SG Opening Promotion - 30pct</RateType>
					<ResId>201115</ResId>
					<TotalRate>508.2000</TotalRate>
					<XNightsDiscount>0.0000</XNightsDiscount>
				</DailyRevenueBreakDown>
			</DailyRevenueBreakDown>
			<DepositAmount>381.1500</DepositAmount>
			<DepositAmountSecond>0.0000</DepositAmountSecond>
			<DepositDateRequired>2019-07-09T00:00:00</DepositDateRequired>
			<DepositRequiredBy>2019-07-09T00:00:00</DepositRequiredBy>
			<DiscountAmount>0.0000</DiscountAmount>
			<DiscountCodeId>0</DiscountCodeId>
			<EventFinish>1900-01-01T00:00:00</EventFinish>
			<EventStart>1900-01-01T00:00:00</EventStart>
			<Extras>0</Extras>
			<ExtrasBalance>0</ExtrasBalance>
			<Fixed>false</Fixed>
			<GroupMaster>false</GroupMaster>
			<GroupMasterAccountId>0</GroupMasterAccountId>
			<GroupResId>0</GroupResId>
			<LastModified>2019-07-09T17:09:34.003</LastModified>
			<ListofCorresspondences/>
			<ListofHouseKeepingTasks/>
			<ListofReceipts>
				<ReceiptLite>
					<DateOfReceipt>2019-07-09T16:43:27.01</DateOfReceipt>
				</ReceiptLite>
			</ListofReceipts>
			<LongTerm>false</LongTerm>
			<MadeBy>RMSOnline</MadeBy>
			<MarketSegment>Public Discount</MarketSegment>
			<MarketSegmentId>16</MarketSegmentId>
			<MealPlanId>0</MealPlanId>
			<ModifiedBy>tallyf.test1</ModifiedBy>
			<Nights>1</Nights>
			<OTARef2>219070900010</OTARef2>
			<POSOnGroupMaster>false</POSOnGroupMaster>
			<Package>0.0000</Package>
			<PaymentAccomm>566.5000</PaymentAccomm>
			<PaymentExtras>0</PaymentExtras>
			<PaymentMode>CTA All Chgs</PaymentMode>
			<PaymentOther>0</PaymentOther>
			<PrimaryClient>
				<AccountBalance>0.0000</AccountBalance>
				<AccountId>7849179</AccountId>
				<ActivitiesDisabled>false</ActivitiesDisabled>
				<AnniversaryDate>1900-01-01T00:00:00</AnniversaryDate>
				<AvgIncome>0</AvgIncome>
				<BlackListed>false</BlackListed>
				<ClientId>7446707</ClientId>
				<ClientType>Client</ClientType>
				<CompId>0</CompId>
				<ConsentToUseCreditCard>false</ConsentToUseCreditCard>
				<CreatedById>0</CreatedById>
				<CreationDate>1900-01-01T00:00:00</CreationDate>
				<DiscountId>0</DiscountId>
				<EDMFilter1OptOut>false</EDMFilter1OptOut>
				<EDMFilter2OptOut>false</EDMFilter2OptOut>
				<EDMFilter3OptOut>false</EDMFilter3OptOut>
				<EmailOptOut>false</EmailOptOut>
				<GDPRPrivacyOptIn>false</GDPRPrivacyOptIn>
				<InsuranceExpDate>1900-01-01T00:00:00</InsuranceExpDate>
				<LastModified>1900-01-01T00:00:00</LastModified>
				<LastTariffTypeId>0</LastTariffTypeId>
				<LastVisit>1900-01-01T00:00:00</LastVisit>
				<MarketingOptOut>false</MarketingOptOut>
				<ModifiedById>0</ModifiedById>
				<NoOfNights>0</NoOfNights>
				<NoOfVisits>0</NoOfVisits>
				<PrefAppartmentId>0</PrefAppartmentId>
				<PrivacyNoMailOuts>false</PrivacyNoMailOuts>
				<RVLengthId>0</RVLengthId>
				<RVSlideId>0</RVSlideId>
				<RVTypeId>0</RVTypeId>
				<RVYear>0</RVYear>
				<ResTypeId>0</ResTypeId>
				<Retailer>false</Retailer>
				<SMSOptOut>false</SMSOptOut>
				<SubResTypeId>0</SubResTypeId>
				<TotalIncome>0</TotalIncome>
			</PrimaryClient>
			<ProjectedRevenue>
				<DailyBreakdown>
					<ProjectedRevenueBreakdown>
						<Discount>0.0000</Discount>
						<Revenue>566.5000</Revenue>
						<Tax>91.5000</Tax>
						<TheDate>2019-07-09T00:00:00</TheDate>
					</ProjectedRevenueBreakdown>
				</DailyBreakdown>
			</ProjectedRevenue>
			<RMSOnlineConfirmationNo>16575244</RMSOnlineConfirmationNo>
			<RateCode>SG Opening Promotion - 30pct</RateCode>
			<RateCodeId>190</RateCodeId>
			<RateOnGroup>true</RateOnGroup>
			<ResTypeId>0</ResTypeId>
			<RoomMoveHeader>false</RoomMoveHeader>
			<RoomMoveHeaderId>0</RoomMoveHeaderId>
			<SecondDepositRequiredBy>1900-01-01T00:00:00</SecondDepositRequiredBy>
			<SecondDiscountAmount>0.0000</SecondDiscountAmount>
			<SubMarketSegmentId>0</SubMarketSegmentId>
			<TariffAmount>420.0000</TariffAmount>
			<TariffOnGroup>false</TariffOnGroup>
			<TariffPosted>true</TariffPosted>
			<TaxAmount>88.2000</TaxAmount>
			<TotalDynamicPrice>0.0000</TotalDynamicPrice>
			<TravelAgent>Sabre SynXis HTNG</TravelAgent>
			<TravelAgentId>2</TravelAgentId>
			<WholesaleId>0</WholesaleId>
		</TPA_Extensions>
		<ResGlobalInfo>
			<HotelReservationIDs>
				<HotelReservationID ResID_Type="14" ResID_Value="64297UT025189"/>
				<HotelReservationID ResID_Type="10"/>
			</HotelReservationIDs>
		</ResGlobalInfo>
	</ReservationsList>
	<ReservationsList>
		<UniqueID Type="4" ID="64297UT025193"/>
		<RoomStays>
			<RoomStay RoomStayStatus="Arrived">
				<RoomTypes>
					<RoomType RoomTypeID="389" RoomTypeCode="0DLX LFS" RoomID="11014">
						<RoomDescription>
							<Text>Long0907101</Text>
						</RoomDescription>
					</RoomType>
				</RoomTypes>
				<GuestCounts>
					<GuestCount AgeQualifyingCode="10" Count="1"/>
					<GuestCount AgeQualifyingCode="8" Count="0"/>
					<GuestCount AgeQualifyingCode="7"/>
				</GuestCounts>
				<TimeSpan End="2019-07-09T17:30:00" Start="2019-07-09T17:20:00"/>
				<Total AmountAfterTax="508.2000"/>
				<BasicPropertyInfo HotelCode="4" HotelName="Lyf Funan - Demo"/>
				<Memberships>
					<Membership ProgramCode=""/>
				</Memberships>
			</RoomStay>
		</RoomStays>
		<ResGuests>
			<ResGuest>
				<Profiles>
					<ProfileInfo>
						<UniqueID Type="" ID="7849179">
							<Profile>
								<Customer>
									<PersonName>
										<GivenName>ACV</GivenName>
										<SurName>Nga</SurName>
									</PersonName>
									<Telehphone PhoneTechType="1"/>
									<Telehphone PhoneTechType="5"/>
									<Address>
										<CityName>Long0907101</CityName>
									</Address>
									<Comments/>
								</Customer>
							</Profile>
						</UniqueID>
					</ProfileInfo>
				</Profiles>
			</ResGuest>
		</ResGuests>
		<TPA_Extensions>
			<Accomm>0.0000</Accomm>
			<AccommBalance>0.0000</AccommBalance>
			<AccountBalance>0.0000</AccountBalance>
			<AccountId>7849199</AccountId>
			<BillCategoryId>0</BillCategoryId>
			<BillToDetails>
				<BillTo>
					<AccType>Accomm</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Accomm</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Extras</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Incidentals</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Gas</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 3</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Electricity</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Bonds</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BillToDetails>
				<BillTo>
					<AccType>Water</AccType>
					<BillTo>Client</BillTo>
					<ContactId>0</ContactId>
					<Description>Folio 5</Description>
					<EntityId>7446707</EntityId>
					<EntityName>Ms Phuong Nga</EntityName>
					<Invoiced>false</Invoiced>
				</BillTo>
			</BillToDetails>
			<BookingSource>Website - ASR</BookingSource>
			<BookingSourceGroupingId>0</BookingSourceGroupingId>
			<BookingSourceId>4</BookingSourceId>
			<BookingType>Accommodation</BookingType>
			<CancelledBy>0</CancelledBy>
			<CancelledDate>1900-01-01T00:00:00</CancelledDate>
			<CategoryId>389</CategoryId>
			<ChargesAccomm>624.8000</ChargesAccomm>
			<ChargesExtras>0</ChargesExtras>
			<ChargesOther>0</ChargesOther>
			<CheckInDate>2019-07-09T17:20:17.893</CheckInDate>
			<CheckOutDate>1900-01-01T00:00:00</CheckOutDate>
			<CommissionPercentage>0.0000</CommissionPercentage>
			<CompanyId>0</CompanyId>
			<ConfirmedBy>2</ConfirmedBy>
			<ConfirmedDate>2019-07-09T17:16:31.607</ConfirmedDate>
			<CreationDate>2019-07-09T17:16:31.467</CreationDate>
			<CurrencyCode>SGD</CurrencyCode>
			<DailyRevenueBreakDown>
				<DailyRevenueBreakDown>
					<AdditionalsAmount>0.0000</AdditionalsAmount>
					<Currency>SGD</Currency>
					<Date>2019-07-09T00:00:00</Date>
					<Discount>0.0000</Discount>
					<DynamicAmount>0.0000</DynamicAmount>
					<ExclusiveTax>88.2000</ExclusiveTax>
					<LinkedRateAdjustmentAmount>-180.0000</LinkedRateAdjustmentAmount>
					<PackageAmount>0.0000</PackageAmount>
					<Period>2019</Period>
					<Rate>600.0000</Rate>
					<RateId>104</RateId>
					<RateTable>SG 600</RateTable>
					<RateType>SG Opening Promotion - 30pct</RateType>
					<ResId>201119</ResId>
					<TotalRate>508.2000</TotalRate>
					<XNightsDiscount>0.0000</XNightsDiscount>
				</DailyRevenueBreakDown>
			</DailyRevenueBreakDown>
			<DepositAmount>381.1500</DepositAmount>
			<DepositAmountSecond>0.0000</DepositAmountSecond>
			<DepositDateRequired>2019-07-09T00:00:00</DepositDateRequired>
			<DepositRequiredBy>2019-07-09T00:00:00</DepositRequiredBy>
			<DiscountAmount>0.0000</DiscountAmount>
			<DiscountCodeId>0</DiscountCodeId>
			<EventFinish>1900-01-01T00:00:00</EventFinish>
			<EventStart>1900-01-01T00:00:00</EventStart>
			<Extras>0</Extras>
			<ExtrasBalance>0</ExtrasBalance>
			<Fixed>false</Fixed>
			<GroupMaster>false</GroupMaster>
			<GroupMasterAccountId>0</GroupMasterAccountId>
			<GroupResId>0</GroupResId>
			<LastModified>2019-07-09T17:47:18.803</LastModified>
			<ListofCorresspondences/>
			<ListofHouseKeepingTasks/>
			<ListofReceipts>
				<ReceiptLite>
					<DateOfReceipt>2019-07-09T17:16:30.92</DateOfReceipt>
				</ReceiptLite>
			</ListofReceipts>
			<LongTerm>false</LongTerm>
			<MadeBy>RMSOnline</MadeBy>
			<MarketSegment>Public Discount</MarketSegment>
			<MarketSegmentId>16</MarketSegmentId>
			<MealPlanId>0</MealPlanId>
			<ModifiedBy>Kiosk</ModifiedBy>
			<Nights>1</Nights>
			<OTARef2>219070900011</OTARef2>
			<POSOnGroupMaster>false</POSOnGroupMaster>
			<Package>0.0000</Package>
			<PaymentAccomm>624.8000</PaymentAccomm>
			<PaymentExtras>0</PaymentExtras>
			<PaymentMode>CTA Rm/Bfst</PaymentMode>
			<PaymentOther>0</PaymentOther>
			<PrimaryClient>
				<AccountBalance>0.0000</AccountBalance>
				<AccountId>7849179</AccountId>
				<ActivitiesDisabled>false</ActivitiesDisabled>
				<AnniversaryDate>1900-01-01T00:00:00</AnniversaryDate>
				<AvgIncome>0</AvgIncome>
				<BlackListed>false</BlackListed>
				<ClientId>7446707</ClientId>
				<ClientType>Client</ClientType>
				<CompId>0</CompId>
				<ConsentToUseCreditCard>false</ConsentToUseCreditCard>
				<CreatedById>0</CreatedById>
				<CreationDate>1900-01-01T00:00:00</CreationDate>
				<DiscountId>0</DiscountId>
				<EDMFilter1OptOut>false</EDMFilter1OptOut>
				<EDMFilter2OptOut>false</EDMFilter2OptOut>
				<EDMFilter3OptOut>false</EDMFilter3OptOut>
				<EmailOptOut>false</EmailOptOut>
				<GDPRPrivacyOptIn>false</GDPRPrivacyOptIn>
				<InsuranceExpDate>1900-01-01T00:00:00</InsuranceExpDate>
				<LastModified>1900-01-01T00:00:00</LastModified>
				<LastTariffTypeId>0</LastTariffTypeId>
				<LastVisit>1900-01-01T00:00:00</LastVisit>
				<MarketingOptOut>false</MarketingOptOut>
				<ModifiedById>0</ModifiedById>
				<NoOfNights>0</NoOfNights>
				<NoOfVisits>0</NoOfVisits>
				<PrefAppartmentId>0</PrefAppartmentId>
				<PrivacyNoMailOuts>false</PrivacyNoMailOuts>
				<RVLengthId>0</RVLengthId>
				<RVSlideId>0</RVSlideId>
				<RVTypeId>0</RVTypeId>
				<RVYear>0</RVYear>
				<ResTypeId>0</ResTypeId>
				<Retailer>false</Retailer>
				<SMSOptOut>false</SMSOptOut>
				<SubResTypeId>0</SubResTypeId>
				<TotalIncome>0</TotalIncome>
			</PrimaryClient>
			<ProjectedRevenue>
				<DailyBreakdown>
					<ProjectedRevenueBreakdown>
						<Discount>0.0000</Discount>
						<Revenue>679.8000</Revenue>
						<Tax>94.8000</Tax>
						<TheDate>2019-07-09T00:00:00</TheDate>
					</ProjectedRevenueBreakdown>
				</DailyBreakdown>
			</ProjectedRevenue>
			<RMSOnlineConfirmationNo>16575730</RMSOnlineConfirmationNo>
			<RateCode>SG Opening Promotion - 30pct</RateCode>
			<RateCodeId>190</RateCodeId>
			<RateOnGroup>true</RateOnGroup>
			<ResTypeId>0</ResTypeId>
			<RoomMoveHeader>false</RoomMoveHeader>
			<RoomMoveHeaderId>0</RoomMoveHeaderId>
			<SecondDepositRequiredBy>1900-01-01T00:00:00</SecondDepositRequiredBy>
			<SecondDiscountAmount>0.0000</SecondDiscountAmount>
			<SubMarketSegmentId>0</SubMarketSegmentId>
			<TariffAmount>420.0000</TariffAmount>
			<TariffOnGroup>false</TariffOnGroup>
			<TariffPosted>true</TariffPosted>
			<TaxAmount>88.2000</TaxAmount>
			<TotalDynamicPrice>0.0000</TotalDynamicPrice>
			<TravelAgent>Sabre SynXis HTNG</TravelAgent>
			<TravelAgentId>2</TravelAgentId>
			<WholesaleId>0</WholesaleId>
		</TPA_Extensions>
		<ResGlobalInfo>
			<HotelReservationIDs>
				<HotelReservationID ResID_Type="14" ResID_Value="64297UT025193"/>
				<HotelReservationID ResID_Type="10"/>
			</HotelReservationIDs>
		</ResGlobalInfo>
	</ReservationsList>
</OTA_ResRetrieveRS>